import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-department-details',
  template: `
<p>
  <button (click)="overview()">overview</button>
  <button (click)="contact()">contact</button>
</p>

  <router-outlet></router-outlet>
    <p>
      department-details works! and the id is: {{deptId}}
      <a (click)="next()">Next</a><br>
      <a (click)="previous()">Previous</a>
    </p>
  `,
  styles: []
})
export class DepartmentDetailsComponent implements OnInit {
    public deptId:any;
  constructor(private currentRoute: ActivatedRoute, private route: Router) { }

  ngOnInit() {
    // let id = parseInt(this.currentRoute.snapshot.paramMap.get('id'));
    // this.deptId = id;

    this.currentRoute.paramMap.subscribe((params: ParamMap)=>{
      this.deptId = parseInt(params.get('id'));
    })
  }

  previous(){
    this.route.navigate(['/department', this.deptId - 1]);
  }

  next(){
    this.route.navigate(['/department', this.deptId + 1]);
  }

  overview(){
    this.route.navigate(['overview'], { relativeTo:this.currentRoute });
  }

  contact(){
    this.route.navigate(['contact'], { relativeTo:this.currentRoute });
  }
}
