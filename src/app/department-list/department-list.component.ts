import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-department-list',
  template: `
<h3>Department List</h3>
<ul>
  <li *ngFor="let dept of department" (click)="selectDepartment(dept.id)">
    <span>{{dept.id}}</span>-{{dept.name}}
  </li>
</ul>
  `,
  styles: []
})
export class DepartmentListComponent implements OnInit {

department = [
  { "id":"1", "name":"Angular" },
  { "id":"2", "name":"TS" },
  { "id":"3", "name":"ES6" },
  { "id":"4", "name":"React" },
  { "id":"5", "name":"CSS" }
]

  constructor(private router: Router) { }

  ngOnInit() {

  }

  selectDepartment(id:any){
      this.router.navigate(['/department', id]);
  }

}
