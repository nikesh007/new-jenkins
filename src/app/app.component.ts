import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  title = "routing-demo";

  ngOnInit(): void {
    // var input = document.querySelector('input');
    // var observable = Observable.fromEvent(input,'input');
    // observable.debounceTime(2000).subscribe({
    //   next: function(event){
    //     console.log(event.target.value);
    //   }
    // })
  }
}
